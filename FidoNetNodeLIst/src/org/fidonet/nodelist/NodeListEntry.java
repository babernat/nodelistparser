package org.fidonet.nodelist;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class NodeListEntry {

	@Id
	protected String id;
	protected String filename;
	protected String sysop;
	protected String location;
	protected String state;
	protected String areaCode;
	protected String phoneNumber;
	protected String name;
	protected String doy;
	protected String year;
	
	
	public NodeListEntry() {
	}
	
	
	
	public String getDoy() {
		return doy;
	}



	public void setDoy(String doy) {
		this.doy = doy;
	}



	public String getYear() {
		return year;
	}



	public void setYear(String year) {
		this.year = year;
	}



	public String getState() {
		return state;
	}



	public void setState(String state) {
		this.state = state;
	}



	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getSysop() {
		return sysop;
	}
	public void setSysop(String sysop) {
		this.sysop = sysop;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
}
