package org.fidonet.nodelist;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.UUID;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class NodeListParser {

	private static SessionFactory sessionFactory;

	static {
		try {
			sessionFactory = new Configuration().configure()
					.buildSessionFactory();
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static void main(String... args) throws IOException {

		for (String filename : args) {
		
			new NodeListParser().parse(filename);
		}

	}

	private static int NODE_NAME = 2;
	private static int LOCATION = 3;
	private static int SYSOP = 4;
	private static int PHONE = 5;

	private void parse(String filename) throws IOException {
		
		File f = new File(filename);
		
		String fn[] = f.getName().split("\\.");
		
		String year = fn[0];
		String doy = fn[2];
		
		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		FileReader reader = new FileReader(filename);

		LineNumberReader lr = new LineNumberReader(reader);

		String line = lr.readLine();

		try {
			while (line != null) {

				try {
					if (!line.startsWith(";")) {
	
						if (line.startsWith(",")) {
							line = "Leaf" + line;
						}
	
						String parts[] = line.split(",");
	
						NodeListEntry entry = new NodeListEntry();
						entry.setFilename(filename);
						entry.setId(UUID.randomUUID().toString());
						entry.setDoy(doy);
						entry.setYear(year);
						
						if (parts[LOCATION].contains("_")) {
							String locationDetails[] = parts[LOCATION].split("_");
							
							if (locationDetails.length == 2) {
								entry.setLocation(locationDetails[0]);
								entry.setState(locationDetails[1]);
							}
							else
								entry.setLocation(parts[LOCATION]);	
						}
						else 
						
							entry.setLocation(parts[LOCATION]);
						entry.setName(parts[NODE_NAME]);
						entry.setSysop(parts[SYSOP]);
	
						String phoneParts[] = parts[PHONE].split("-");
	
						try {
							entry.setAreaCode(phoneParts[1]);
							entry.setPhoneNumber(phoneParts[2] + "-" + phoneParts[3]);
						} catch(IndexOutOfBoundsException e) {
							entry.setPhoneNumber(parts[PHONE]);
						}
	
						session.save(entry);
	
					}
				} catch(IndexOutOfBoundsException e) {
					System.out.println("Could not handle line: " + line);
				}

				line = lr.readLine();
			}
			tx.commit();
		} catch (IOException e) {
			tx.rollback();
			e.printStackTrace();
		}

		session.close();
	}

}
